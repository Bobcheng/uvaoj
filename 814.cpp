#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <set>
//#define LOCAL

using namespace std;
int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif

	int at,num;
	string line,data[500];
	string temp,host,name,sender,receivers[100][3],sendername,senderhost,hostnow;
	map<string, int> host2rank;
	set<string> allhosts[1000];

	for(int rank=0;;rank++){
		getline(cin, line);
		if(line[0]=='*')
			break;
		else{
			stringstream liness(line); //stringstream 的用法，每次都要新建一个，不然会出问题。
			liness>>temp>>host>>num;
			host2rank[host]=rank;
			for(int i = 0; i < num; i++){
				liness>>name;
				allhosts[rank].insert(name);
			}
		}
	}
	while(1){
		int j = 0;
		int count = 0;
		getline(cin, line);
		if(line[0] == '*')
			break;
		else{
			stringstream liness(line);
			liness >> sender;
			at = sender.find('@');
			sendername = sender.substr(0,at);
			senderhost = sender.substr(at+1);
			
			while(liness >> temp){
				at = temp.find('@');
				receivers[j][0] = temp.substr(0,at);  //取子串，第一个参数是子串的起始位置
				receivers[j][1] = temp.substr(at+1); //第二个参数是字串结束的位置，默认为字符串尾
				receivers[j][2] = "n";
				for(int m = 0; m < j; m++){
					if(receivers[m][0] == receivers[j][0]){
						if(receivers[m][1] == receivers[j][1]){
							j--;
							break;
						}
					}
				}
				j++;
			}
		}
		getline(cin, temp);
		for(count = 0; ; count++){
			getline(cin, line);
			if(line[0] == '*')
				break;
			else{
				data[count] = line;
			}
		}

		for(int i = 0; i < j; i++){
			if(receivers[i][2] == "n"){
				int flag = 0;
				hostnow = receivers[i][1];
				cout << "Connection between " << senderhost << " and " << hostnow<<endl;
				cout << "     HELO "<<senderhost<<"\n     250\n";
				cout << "     MAIL FROM:<"<<sender<<">\n     250\n";
				for(int m = i; m < j; m++){
					if(receivers[m][1] == hostnow){
						cout << "     RCPT TO:<"<<receivers[m][0]<<"@"<<receivers[m][1]<<">\n";
						receivers[m][2] = "y";
						if(allhosts[host2rank[hostnow]].find(receivers[m][0])!=allhosts[host2rank[hostnow]].end()){
							cout<<"     250\n";
							flag = 1;
						}else{
							cout<<"     550\n";
						}
					}
				}
				if(flag){
					cout << "     DATA\n     354\n";
					for(int n = 0; n < count; n++){
						cout<< "     " << data[n] << "\n";
					}
					cout << "     .\n     250\n";
				}
				cout << "     QUIT\n     221\n";
			}
			else
				continue;
		} 
	}
	
	return 0;
}