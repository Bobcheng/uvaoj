#include <iostream>
#include <vector>
#include <algorithm>
//#define LOCAL
using namespace std;

struct Building{
	int id;
	double x, y, w, d, h;
	
	bool operator < (const Building &rhs) const{  
		return x < rhs.x || (x == rhs.x && y < rhs.y) ;
	} //结构体内部定义小于符号的含义，用来比较两个对象的大小，方便排序使用。
	/*
	const类型的引用变量表示该变量不会被改变。
	函数之后的const表示该函数是静态函数（常成员函数）。能够访问但不会改变成员变量，不能调用类中任何非const成员函数。
	*/
};

vector<Building> bds;
vector<double> xs; //定义为全局变量方便其他函数调用。一般需要查阅的都定义为全局变量。

bool covor (Building b, double mx){
	return b.x <= mx && b.x + b.w >= mx;
}

bool visible(Building b, double mx){
	if(covor(b,mx)){
		for(Building bd : bds){   //循环的方法，适用于vector
			if(bd.y<b.y && covor(bd,mx) && bd.h>=b.h)
				return false;
		}
		return true;
	}
	else
		return false;
}



int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif

	int kase = 0,n;
	Building b;
	while(cin>>n && n!=0){
		for(int i = 0; i < n; i++){
			cin>>b.x>>b.y>>b.w>>b.d>>b.h;
			b.id = i+1;
			bds.push_back(b);
			xs.push_back(b.x);
			xs.push_back(b.x+b.w);
		}
		sort(bds.begin(),bds.end());
		sort(xs.begin(),xs.end());
		int m = unique(xs.begin(),xs.end()) - xs.begin();
		//使用unique去重之前要先排序，去重实际上是将重复的移到了后面，返回的值是无重复的部分的尾指针+1。减去头指针可以得到无重复元素个数。

		if(kase++) cout<<'\n';
		cout<<"For map #"<<kase<<", the visible buildings are numbered as follows:\n";
		for(Building b : bds){
			bool vis = false;
			for(int i = 0; i < m-1; i++){
				if(visible(b, (xs[i]+xs[i+1])/2)){
					vis = true;
					break;
				}
			}
			if(vis){
				if(b.id != bds[0].id)
					cout<<" ";
				cout<<b.id;
			}
		}
		cout << endl;
		bds.clear();
		xs.clear();//！！太重要了，因为需要循环，执行完一次之后需要清空内容留给下一次使用
	}
	return 0;
}