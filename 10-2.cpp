#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LOCAL

char map[15][15];

int barrier(int a,int b,int ver,int par);
int issave(int r,int c);
int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
	int count,G_r,G_c;
	char tt;
	while(scanf("%d%d%d%c",&count,&G_r,&G_c,&tt)==4&&count!=0){
		memset(map,'\0',sizeof(map));
		for(int i=0;i<count;i++){
			int row,col;
			char ch;
			if(scanf("%c%d%d%c",&ch,&row,&col,&tt)==4)
				map[row][col]=ch;
			else
				printf("error\n");
		}	
		int flag=0;
		if(issave(G_r-1,G_c)||issave(G_r+1,G_c)||issave(G_r,G_c+1)||issave(G_r,G_c-1))
			flag=1;
		flag==1?printf("NO\n"):printf("YES\n");
		
	}
	return 0;
}

int issave(int r,int c){
	char temp = map[r][c];
	map[r][c] = '\0';
	if(r<1||r>3||c<4||c>6)
		return 0;
	for(int i=1;i<=10;i++){
		switch (map[i][c]){
			case 'R':
				if(barrier(i,r,1,c)==0)
					return 0;
				break;
			case 'G':
				if(barrier(i,r,1,c)==0)
					return 0;
				break;
			case 'C':
				if(barrier(i,r,1,c)==1)
					return 0;
				break;
		}
	}
	for(int i=1;i<=9;i++){
		switch (map[r][i]){
			case 'R':
				if(barrier(i,c,0,r)==0)
					return 0;
				break;
			case 'C':
				if(barrier(i,c,0,r)==1)
					return 0;
				break;
		}
	}
	if(r-2>0){
		if(map[r-2][c-1]=='H'&&map[r-1][c-1]=='\0')
			return 0;
		if(map[r-2][c+1]=='H'&&map[r-1][c+1]=='\0')
			return 0;
	}
	if(r-1>0){
		if(map[r-1][c-2]=='H'&&map[r-1][c-1]=='\0')
			return 0;
		if(map[r-1][c+2]=='H'&&map[r-1][c+1]=='\0')
			return 0;
	}
	if(map[r+2][c-1]=='H'&&map[r+1][c-1]=='\0')
		return 0;
	if(map[r+2][c+1]=='H'&&map[r+1][c+1]=='\0')
		return 0;
	if(map[r+1][c-2]=='H'&&map[r+1][c-1]=='\0')
		return 0;
	if(map[r+1][c+2]=='H'&&map[r+1][c+1]=='\0')
		return 0;
	map[r][c]=temp;
	return 1;
}

int barrier(int a,int b,int ver,int par){
	int sum=0;
	if(ver){
		if(b>a){
			for(int i=a+1;i<b;i++){
				if(map[i][par]!='\0')
					sum++;
			}
		}
		else{
			for(int i=b+1;i<a;i++){
				if(map[i][par]!='\0')
					sum++;
			}
		}
	}
	else{
		if(b>a){
			for(int i=a+1;i<b;i++){
				if(map[par][i]!='\0')
					sum++;
			}
		}
		else{
			for(int i=b+1;i<a;i++){
				if(map[par][i]!='\0')
					sum++;
			}
		}
	}
	return sum;
}

