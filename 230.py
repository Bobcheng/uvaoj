shelf = []
borrow = []
bkreturn = []

def getbk():
    while True:
        book = tuple(input().split(" by "))
        if book[0][0] == 'E':
            break
        shelf.append(book)

def sortbk(books):
    books.sort(key = lambda book: (book[1], book[0][1:]))


def borrowbk(bookname):
    for book in shelf:
        if book[0] == bookname:
            borrow.append(book)
            shelf.remove(book)
            break

def returnbk(bookname):
    for book in borrow:
        if book[0] == bookname:
            bkreturn.append(book)
            borrow.remove(book)
            break

def findlastbk(shelf, book):
    for i in range(len(shelf)):
        if shelf[i] == book:
            if i == 0:
                return None
            return shelf[i-1]


def shelve():
    sortbk(bkreturn)
    for book in bkreturn:
        shelf.append(book)
        sortbk(shelf)
        lastbook = findlastbk(shelf, book)
        if lastbook == None:
            print('Put %s first' % book[0])
        else:
            print('Put %s after %s' % (book[0], lastbook[0]))
    bkreturn.clear()
    print("END")

getbk()
while True:
    line = input()
    if line[0] == 'E':
        break
    command = line[:6]
    bookname = line[7:]
    if command[0] == 'B':
        borrowbk(bookname)
    elif command[0] == 'R':
        returnbk(bookname)
    elif command[0] == 'S':
        shelve()

