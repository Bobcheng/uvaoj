#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <algorithm>
#define LOCAL

#define ALL(x) x.begin(),x.end()
#define INS(x) inserter(x,x.begin())
using namespace std;

typedef set<int> Set;
map<Set,int> set2id;
vector<Set> id2set;
stack<int> stk;

int getID(Set x){
	if(set2id.count(x))
		return set2id[x];
	id2set.push_back(x);
	set2id[x]=id2set.size()-1;
	return set2id[x];
}

#define ALL(x) x.begin(),x.end()
#define INS(x) inserter(x,x.begin())

int main(){
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
	int round,optnum;
	cin>>round;
	for(int m=0;m<round;m++){
		cin>>optnum;
		for(int n=0;n<optnum;n++){
			string opt;
			cin>>opt;
			if(opt[0]=='P'){
				stk.push(getID(Set()));
			}
			else if(opt[0]=='D'){
				stk.push(stk.top());
			}
			else{
				Set x1=id2set[stk.top()];stk.pop();
				Set x2=id2set[stk.top()];stk.pop();
				Set x;
				if(opt[0]=='U'){
					set_union(ALL(x1),ALL(x2),INS(x));
				}
				else if(opt[0]=='I'){
					set_intersection(ALL(x1),ALL(x2),INS(x));
				}
				else if(opt[0]=='A'){
					x=x2;
					x.insert(getID(x1));
				}
				stk.push(getID(x));
			}
			cout<<id2set[stk.top()].size()<<endl;
		}
		cout<<"***"<<endl;
	}
	return 0;

}