#include <stdlib.h>
#include <stdio.h>
#define LOCAL
int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
		int num=1;
	while(1){

		int dots=0,lines=0;
		char temp;
		
		if(scanf("%d",&dots)!=1)
			break;
		scanf("%d",&lines);
		scanf("%c",&temp);
		int nums[dots]={0};
		int rowmap[dots][dots];
		int colmap[dots][dots];
		for(int i=0;i<dots;i++){
			for(int j=0;j<dots;j++){
				rowmap[i][j]=0;
				colmap[i][j]=0;
			}
		}

		for(int i=0;i<lines;i++){
			char str[10]={'\0'};
			fgets(str,10,stdin);
			switch(str[0]){
				case 'H':
					rowmap[str[2]-49][str[4]-49]=1;
					break;
				case 'V':
					colmap[str[4]-49][str[2]-49]=1;
					break;
				default:
					printf("error\n");
					break;
			}
		}
		

		for(int i=1;i<=dots-1;i++){
			for(int j=0;j<dots-i;j++){
				for(int k=0;k<dots-i;k++){
					int flag=1;
					for(int m=0;m<i;m++){
						if(rowmap[j][k+m]!=1||rowmap[j+i][k+m]!=1||colmap[j+m][k]!=1||colmap[j+m][k+i]!=1){
							flag=0;
						}
					}
					if(flag){
						nums[i]+=1;		
					}
				}
			}
		}

		if(num!=1){
			printf("\n**********************************\n\n");
		}
		printf("Problem #%d\n\n",num++);
		int flag2=0;
		for(int i=1;i<=dots-1;i++){
			if(nums[i]!=0){
				printf("%d square (s) of size %d\n",nums[i],i );
				flag2=1;
			}
			if(!flag2){
				printf("No completed squares can be found.\n");
			}
			
		}

		



	}
	return 0;
}

