#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
//#define LOCAL

using namespace std;
int minn = 10000000000;
int leaf = -1;
struct Node{
	Node * left;
	Node * right;
	int weight;
	int allwt;
};

vector<int> subarray(vector<int> arr, int a, int b){
	vector<int> newv;
	newv.clear();
	for(int i = a; i < b; i++){
		newv.push_back(arr[i]);
	}
	return newv;
}

int find(vector<int> v, int k){
	for(int i = 0; i < v.size(); i++){
		if(v[i] == k)
			return i;
	}
	return -1;
}


void buildTree(Node * &root, vector<int> mid, vector<int> post){
	if(!mid.empty()){
		root = new Node();
		int m = post.back();
		root->weight = m;
		root->left = NULL;
		root->right = NULL;
		int num = find(mid, m);
		vector<int> new_midl =subarray(mid, 0, num);
		vector<int> new_midr = subarray(mid, num+1, mid.size());
		vector<int> new_postl = subarray(post, 0,num);
		vector<int> new_postr = subarray(post, num, mid.size()-1);
		buildTree(root->left, new_midl, new_postl);
		buildTree(root->right, new_midr, new_postr);
	}
}

void checkTree(Node * root, int parentwt){
	if(root){
		root->allwt = root->weight + parentwt;
		if((!root->left) && (!root->right)){
			if(root->allwt < minn || (root->allwt == minn && root->weight < leaf)){
				minn = root->allwt;
				leaf = root->weight;
			}
		}
		checkTree(root->left, root->allwt);
		checkTree(root->right, root->allwt);
	}

}




int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt", "r", stdin);
		freopen("456.txt", "w", stdout);
	#endif
	vector<int> mid;
	vector<int> post;
	string line1, line2;
	int temp1,temp2;
	while(1){
		if(!getline(cin, line1))
			break;
		getline(cin, line2);
		stringstream ss1(line1);
		stringstream ss2(line2);
		while(ss1 >> temp1 && ss2 >> temp2){
			mid.push_back(temp1);
			post.push_back(temp2);
		}
		Node * root = NULL;
		buildTree(root, mid, post);
		checkTree(root, 0);
		cout << leaf << endl;
		mid.clear();
		post.clear();
		minn = 10000000000;
		leaf = -1;
	}
	
	return 0;
}