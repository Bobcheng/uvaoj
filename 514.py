def check(inQ, outQ):
    stdQ = inQ
    stack = []
    for out in outQ:
        if len(stack) != 0 and stack[len(stack) - 1] == out:
            stack.pop()
            continue
        else:
            flag = 0
            for inn in inQ:
                if inn != out:
                    stack.append(inn)
                else:
                    flag = 1
                    inQ = stdQ[inn:]
                    break
            if not flag:
                return False
    return True

while True:
    n = int(input())
    if n == 0:
        break
    while True:
        inQ = list(range(1,n+1))
        outQ = list(map(int,input().split(' ')))  # [5,4,3,2,1]
        if outQ[0] == 0:
            print()
            break
        else:
            if check(inQ, outQ):
                print("Yes")
            else:
                print("No")