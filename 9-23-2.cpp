#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define LOCAL
int readcodes();
//int readchar();
int readint(int c);
char code [8][1<<8];

int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
	while(readcodes()){
		for(;;){
			int len=readint(3);
			if(len==0) break;
			for(;;){
				int v=readint(len);
				if(v==((1<<len)-1)) {
					break;
				}
				putchar(code[len][v]);
			}
		}
		getchar();
		printf("\n");
	}

	return 0;
}

int readcodes(){
	memset(code,0,sizeof(code));
	for(int i=1;i<=7;i++){
		int flag=1;
		for(int j=0;j<=((1<<i)-2);j++){
		//	if((code[i][j]=getchar())==EOF)
			if(scanf("%c",&code[i][j])!=1)
				return 0;
			if(code[i][j]=='\n'){
				flag=0;
				break;
			}
				
		}
		if(flag==0)
			break;
	}

	return 1;
}

int readint(int c){
	int sum=0,a,ch;
	for(int i=1;i<=c;i++){
		//ch=getchar();
		scanf("%c",&ch);
		if(ch=='\n'||ch=='\r')
			i--;
		else{
			a=ch-'0';
			sum+=a<<(c-i);
		}
	}
	return sum;
}