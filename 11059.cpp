#include<cstdio>
#include<cstdlib>
#include<iostream>
#define LOCAL
using namespace std;

int main (int argc, char const *argv[]) {
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
    int total;  
    int kase = 0;  
    while(scanf("%d", &total) == 1 && total){
        int numbers[20];
        long long products;
        // cin >> total;
        // if(!total)
        //     break;
        for (int i = 0; i < total; i++){
            cin >> numbers[i];
        }
        long long products_max = 0;
        for(int m = 0; m < total; m++){
            for(int n = m; n < total; n++){
                if(m == n && numbers[m] <= 0)
                    products = 0;
                else{
                    products = 1; 
                    for(int i = m; i <= n; i++){
                        products *= numbers[i];
                    }
                    if(products < 0){
                        products = 0;
                    }
                }
                if (products > products_max){
                    products_max = products;
                }
            }
        }
        cout<<"Case #"<<++kase<<": The maximum product is "<<products_max<<".\n\n";
    }
    return 0;
}