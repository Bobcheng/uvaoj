allarray = {}
count = 0
bug = 0

def arrayvalue (string):
    if string[0] >= '0' and string[0] <= '9':
        return int(string)
    first = string.find('[')
    name = string[:first]
    inner = string[first+1:-1]
    if inner[0] < '0' or inner[0] > '9':
        if arrayvalue(inner) == None:
            return None
        else:
            inner = str(arrayvalue(inner))
    out = allarray[name].get(int(inner))
    return out

def arrayindex(string):
    first = string.find('[')
    name = string[:first]
    inner = string[first + 1:-1]
    if inner[0] < '0' or inner[0] > '9':
        return arrayvalue(inner)
    else:
        return int(inner)

def init(string):
    first = string.find('[')
    name = string[:first]
    inner = string[first + 1:-1]
    allarray[name] = {'length': int(inner)}

while True:
    line = input()
    count += 1
    if line[0] == '.':
        print(bug)
        allarray = {}
        count = 0
        bug = 0
        line = input()
        count += 1
        if line[0] == '.':
            break
    if line.find('=') == -1:
        init(line)
    else:
        l = line.split('=')
        left, right = l[0], l[1]
        first = left.find('[')
        name = left[:first]
        leftindex = arrayindex(left)
        rightvalue = arrayvalue(right)
        if bug == 0 and (leftindex == None or\
            rightvalue == None or\
                leftindex >= allarray[name]['length']):
            bug = count
        else:
            allarray[name][leftindex] = rightvalue












