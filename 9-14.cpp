#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define LOCAL

void prtmap(char (*map)[10]);
int move(char x,char (*map)[10]/*or:char map[][6]*/,int* row,int* col);

int main(int argc, char const *argv[]){
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
		int num=1;
while(1){
		int *row=(int*)malloc(sizeof(int)), *col=(int*)malloc(sizeof(int));
		char map[5][10]={'\0'};
		char instruction[1000]={'\0'};
		int islast=0;
		for(int i=0;i<5;i++){
			fgets(map[i],10,stdin);//scanf 和 gets 会自动消去行末的\n,但是fgets会保留在字符数组里!
			//scanf("%s",map[i]);
			//gets(map[i]);
			if(strlen(map[i])<=3){
				islast=1;
				break;
			}
		}
		if(islast) break;
		
		for(int i=0;i<5;i++){
			for (int j = 0; j < 5; ++j){
				if(map[i][j]==' '){
					*row=i;*col=j;
				}
			}
		}
		for(int i=0;;i++){
			char temp;
			scanf("%c",&temp);
			if(temp>=65&&temp<=90){
				instruction[i]=temp;
				continue;
			}
			else if(temp=='0'){
				instruction[i]=temp;
				scanf("%c",&temp);
				break;
			}
			else
				i--;
		}
		if(num!=1){
			printf("\n");
		}
		printf("Puzzle #%d:\n",num++);
		int flag=1;
		for(int i=0;instruction[i]!='0';i++){
			if(!move(instruction[i],map,row,col)){
				printf("This puzzle has no final configuration.\n");
				flag=0;
				break;
			}
		}
		if(flag){
			prtmap(map);
		}
	}
	return 0;

}

void prtmap(char (*map)[10]){
	for(int i=0;i<5;i++){
		for(int j=0;j<5;j++){
			j==0?printf("%c",map[i][j]):printf(" %c",map[i][j]);
		}
		printf("\n");
	}
}

int move(char x,char (*map)[10]/*or:char map[][6]*/,int* row,int* col){
//数组指针对应的是二维数组，指针对应的是数组
	switch(x){
		case 65:
			if((*row)==0)
				return 0;
			else
				map[*row][*col]=map[*row-1][*col];
				map[*row-1][*col]=' ';
				(*row)--;
				break;
		case 66:
			if((*row)==4)
				return 0;
			else
				map[*row][*col]=map[*row+1][*col];
				map[*row+1][*col]=' ';
				(*row)++;
				break;
		case 76:
			if((*col)==0)
				return 0;
			else
				map[*row][*col]=map[*row][*col-1];
				map[*row][*col-1]=' ';
				(*col)--;
				break;
		case 82:
			if((*col)==4)
				return 0;
			else
				map[*row][*col]=map[*row][*col+1];
				map[*row][*col+1]=' ';
				(*col)++;
				break;
		default:
				return 0;

	}
	return 1;
}