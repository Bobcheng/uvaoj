while True:
    n = int(input())
    if n == 0:
        break
    else:
        cards = list(range(1, n+1))
        print('Discarded cards:', end='')
        while len(cards) > 1:
            if len(cards) != 2:
                print(' '+str(cards.pop(0))+',', end='')
            else:
                print(' ' + str(cards.pop(0)), end='')
            cards.append(cards.pop(0))
        print()
        print('Remaining card: '+str(cards[0]))