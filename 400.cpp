#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#define LOCAL
using namespace std;

void pblank(int a);
int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
	int num=0;
	while(scanf("%d",&num)==1){
		vector<string> v;
		string str,temps;
		int longest=0,col,row,index,flength,blanks;
		for(int i = 0; i < num; i++){
			cin>>str;
			if(str.length() > longest)
				longest=str.length();
			v.push_back(str);
		}
		sort(v.begin(), v.end());
		col = ( 60 - longest ) / ( longest + 2 ) + 1;
		row = num % col == 0? num / col : num / col + 1;
		for(int i = 0; i < 60; i++){
			cout<<'-';
		}
		cout<<'\n';
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				index=i + j * row;
				if(index<num){
					temps = v.at(index);
					flength = temps.length();
					j == col - 1 ? blanks = longest - flength : blanks = longest - flength + 2;
					cout<<temps;
					pblank(blanks);
				}
			}
			cout<<'\n';
		}

	}
	return 0;
}

void pblank(int a){
	for(int i = 0; i < a; i++){
		cout<<' ';
	}
} 