zhou = None


def check (line):
    distance = list(map(lambda t: t[0] - zhou, line ))
    if sum(distance) == 0:
        return True
    else:
        return False

def detect (allpoint):
    line = []
    for j in range(n):
        line.append(allpoint[j])
        if j == n - 1 or allpoint[j][1] != allpoint[j + 1][1]:
            if not check(line):
                return False
            else:
                line.clear()
    return True



kase = int(input())
for i in range(kase):
    n = int(input())
    allpoint = []
    for j in range(n):
        lstr = input()
        point = tuple(map(int, lstr.split(' ')))
        allpoint.append(point)
    allpoint.sort(key = lambda p: (p[1],p[0]))
    for j in range(n):
        if allpoint[j][1] != allpoint[0][1]:
            zhou = (allpoint[j-1][0]+allpoint[0][0]) / 2
            break
    if (zhou == None):
        zhou = (allpoint[0][0] + allpoint[n - 1][0]) / 2
    if detect(allpoint):
        print('YES')
    else:
        print('NO')
