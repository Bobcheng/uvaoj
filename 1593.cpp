#include <iostream>
#include <string>
#include <sstream>
#include <vector>
//#define LOCAL

using namespace std;

vector<vector<string> >  file;
vector<int> 		vmax;
int cols = 0;
int rows = 0;

void addline (stringstream &ssline){
	vector<string> vline;
	string temp;
	while(ssline >> temp){
		vline.push_back(temp);
	}
	if(vline.size() > cols){
		cols = vline.size();
	}
	file.push_back(vline);
}

void findmax (){
	int max = 0;
	rows = file.size();
	for(int i = 0; i < cols; i++){
		max = 0;
		for(int j = 0; j < rows; j++){
			if(i<file[j].size()){
				if(file[j][i].length() > max){
					max = file[j][i].length();
				}
			}
		}
		vmax.push_back(max);
	}
}

void blanks (int num){
	for(int i = 0; i < num; i++){
		cout << " ";
	}
}

void printout (){
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			if(j+1 < file[i].size()){
				cout << file[i][j];
				blanks(vmax[j]+1-file[i][j].length());
			}else{
				cout << file[i][j];
				break;
			}
		}
		cout<<endl;
	}
}


int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt", "r", stdin);
		freopen("456.txt", "w", stdout);
	#endif
	string sline;
	while(1){
		getline(cin, sline);	
		if(sline.length() == 0)
			break;
		else{
			stringstream ssline(sline);
			addline(ssline);
		}	
	}
	findmax();
	printout();

	return 0;
}