def checknum(queue, pos):
    myfile = queue[pos]
    n = 0
    rankq = list(queue[:])
    rankq.sort(reverse=True)
    while True:
        for i in range(len(queue)):
            if queue[i] != rankq[0]:
                continue
            elif queue[i] == rankq[0]:
                queue[i] = -1
                rankq.remove(rankq[0])
                n += 1
                if i == pos:
                    return  n


kase = int(input())
for m in range(kase):
    line1 = list(map(int, input().split(' ')))
    queue = list(map(int, input().split(' ')))
    myfile = queue[line1[1]]
    n = checknum(queue, line1[1])
    print(n)

