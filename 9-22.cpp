#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define LOCAL
#define maxn 100
int left, chance;
char s[maxn],s2[maxn];
int win,lose;
int guessed[256];

void guess(char ch);

int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt","r",stdin);
		freopen("456.txt","w",stdout);
	#endif
	int rnd;
	while(scanf("%d%s%s",&rnd,s,s2) == 3 && rnd != -1){
		printf("Round %d\n",rnd);
		for(int i=0;i<256;i++)
			guessed[i]=0;
		win=lose=0;
		left = strlen(s);
		chance = 7;
		for(int i=0;i<strlen(s2);i++){
			guess(s2[i]);
			if(win||lose)
				break;
		}

		if(win) printf("You win.\n");
		else if(lose) printf("You lose.\n");
		else printf("You chickened out.\n");
	}
	return 0;
}

void guess(char ch){
	int bad=1;
	if(guessed[ch]==0){
		guessed[ch]=1;
		for(int i=0;i<strlen(s);i++){
			if(s[i]==ch){
				bad=0;
				s[i]=' ';
				left--;
			}
		}
	}	
	
	if(bad)
		chance--;
	if(!chance)
		lose=1;
	if(!left)
		win=1;
}