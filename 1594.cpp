#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
//#define LOCAL
using namespace std;

vector<int> next(const vector<int> &v){
	vector<int> out;
	int temp;
	for (int i = 0; i < v.size(); ++i){
		temp = abs(v[i] - v[(i+1)%v.size()]);
		out.push_back(temp);
	}
	return out;
}

bool issame(const vector<int> &a, const vector<int> &b){
	for (int i = 0; i < a.size(); ++i){
		if(a[i] != b[i])
			return false;
	}
	return true;
}

string check(vector<int> origin){
	string result;
	bool zero = true;
	vector<vector<int> > all;
	all.push_back(origin);
	for(int i = 0; i < 1000; i++){
		all.push_back(next(all.back()));
	}

	vector<int> back = all.back();
	for(int a : back){
		if(a != 0){
			zero = false;
			break;
		}
	}
	if(zero) return "ZERO";
	else return "LOOP";
	// for(int i = all.size()-2; i > -1; i--){
	// 	if(issame(back,all[i])){
	// 		return "LOOP";
	// 	}
	// }

}


int main(int argc, char const *argv[])
{
	#ifdef LOCAL
		freopen("123.txt", "r", stdin);
		freopen("456.txt", "w", stdout);
	#endif
	int num = 0, count = 0, temp;
	vector<int> origin;
	cin >> num;
	for(int i = 0; i < num; i++){
		cin >> count;
		origin.clear();
		for (int j = 0; j < count; ++j){
			cin >> temp;
			origin.push_back(temp);
		}
		cout << check(origin) << endl;	
	}
	return 0;
}